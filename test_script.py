#!/usr/bin/env python
# coding: utf-8

# # Import Connectors

# In[1]:


# Run this cell
from salesforce_comp_base_library.api.api import Org62
from salesforce_comp_base_library.chrome_browser.mycookies import MyCookies

# import webbrowser library


# In[2]:


config_file = r"C:\Users\ctrixner\Downloads\config.ini"

import configparser

config = configparser.ConfigParser()
config.read(config_file)

cookies_path = config.get('browser', 'cookie_file')

# In[3]:


chrome_cookie_file = f"{cookies_path}\\Default\\Network\\Cookies"
chrome_local_file = f"{cookies_path}\\Local State"
my_cookies = MyCookies(chrome_cookie_file, chrome_local_file)
org62_sid = my_cookies.df_host_key_by_cookie_name(
    'org62.my.salesforce.com', ['sid']).iloc[0]['value']
con = Org62(org62_sid)

df = con.query_to_dataframe("select id from order limit 1")

df.to_csv(r"C:\Users\ctrixner\Downloads\test.csv")

# In[ ]:
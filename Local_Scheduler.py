#!/usr/bin/env python
# coding: utf-8

# ## Install Library
# 
# 1.  Download library from GitLab.
# 2.  Get the path of the download location including the file name.
#     1.  An example would be "C:\Users\ctrixner\Downloads\salesforce_comp_base_library-gic.zip"
# 3.  Run the below cell and replace it with your file path.

# In[ ]:





# # Import Connectors

# In[1]:


import time
import traceback
from pathlib import Path
import os
import pandas as pd
from salesforce_comp_base_library.chrome_browser.pages import WebPageHandler, browser


# In[2]:


#FUNCTIONS

def update_user_config_with_latest_cookies(config_file):
    import os
    import configparser
    directory = os.path.join(str(Path.home()),'AppData','Local','Temp')
    files = [dp for dp, dn, filenames in os.walk(directory)if os.path.split(dp)[-1].startswith('scoped')]
    files.sort(key=lambda x: os.path.getmtime(x))
    latest_file = files[-1]
    config = configparser.ConfigParser()
    config.read(config_file)
    if not config.has_option('browser', 'cookie_file'):
        config.add_section('browser')
        config.set('browser', 'cookie_', 'spam')
        
    config.set('browser', 'cookie_file',latest_file)

    with open(config_file, 'w') as configfile:
        config.write(configfile)

def read_scheduler():
    df = pd.read_csv(
        SCHEDULED_JOBS)
    return df

def get_active_jobs():
    df = read_scheduler()
    import datetime
    return df[(pd.to_datetime(
        df['date']) <= datetime.datetime.now().strftime(
        '%Y-%m-%d %H:%M:%S')) & (df['status'] == 'PENDING')].sort_values(by=['date'])

def update_status(job_id, status):
    df = pd.read_csv(
       SCHEDULED_JOBS)
    index_value = df[df['id'] == job_id]['status'].index[0]
    df.at[index_value, 'status'] = status
    df.to_csv(
       SCHEDULED_JOBS,
        index=False)


# In[3]:


#VARIABLES
SCHEDULED_JOBS = r"C:\Users\ctrixner\Downloads\scheduled_jobs.csv"
CONFIG_FILE = r"C:\Users\ctrixner\Downloads\config.ini"


# https://chromedriver.chromium.org/downloads
CHROME_EXECUTABLE_PATH = r"C:\Users\ctrixner\PycharmProjects\salesforce_comp\salesforce_comp\browser\resources\chromedriver.exe"


# In[4]:


#GENERATE FILES
if not os.path.exists(CONFIG_FILE):
    f = open(CONFIG_FILE, "x")
    f.close()
    
if not os.path.exists(SCHEDULED_JOBS):
    pd.DataFrame(columns=['id','date','name','path','status','comment']).to_csv(SCHEDULED_JOBS, index=False)


# In[ ]:


driver = browser(executable_path=CHROME_EXECUTABLE_PATH,
                         headless=False)

WebPageHandler(driver).refresh_pages()

update_user_config_with_latest_cookies(CONFIG_FILE)


while True:

    WebPageHandler(driver).refresh_pages()
    
    df_scheduler = get_active_jobs()

    if len(df_scheduler) == 0:
        time.sleep(10)
        continue

    for idx, job in df_scheduler.iterrows():
        job_id = job['id']
        script_filepath = job['path']
        name = job['name']


        while True:
            try:
                exec(open(script_filepath).read())
                update_status(job_id, "COMPLETED")
                print(f"{job_id} COMPLETED")
                break
            except Exception as e:
                traceback.print_exc()
                update_status(job_id, e)

    time.sleep(10)


# In[ ]:




